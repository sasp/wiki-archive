# SASP Wiki Archive

[![pipeline status](https://gitlab.com/sasp/wiki-archive/badges/master/pipeline.svg)](https://gitlab.com/sasp/wiki-archive/commits/master)

----

This repository contains a static archive of various Wikis that the
Solar and Stellar Physics Group have used over the years.  Use the
links below to view the archive:

- [asteroSTEP](https://sasp.gitlab.io/wiki-archive/asteroSTEP.html)
  - Last modified: 2016 April 20
  - Archived: 2019 July 18
- [corotgiantsWiki](https://sasp.gitlab.io/wiki-archive/corotgiantsWiki.html)
  - Last modified: 2016 June 3
  - Archived: 2019 July 18
- [hare_and_hounds1_spaceinn](https://sasp.gitlab.io/wiki-archive/hare_and_hounds1_spaceinn.html)
  - Last modified: 2016 January 12
  - Archived: 2019 July 18
- [KascWg1Wiki](https://sasp.gitlab.io/wiki-archive/KascWg1Wiki.html)
  - Last modified: 2014 May 2
  - Archived: 2019 July 18
- [KascWg2Wiki](https://sasp.gitlab.io/wiki-archive/KascWg2Wiki.html)
  - Last modified: 2010 July 26
  - Archived: 2019 July 18
- [platogiantsWiki](https://sasp.gitlab.io/wiki-archive/platogiantsWiki.html)
  - Last modified: 2011 March 10
  - Archived: 2019 July 18

----

The following wget incantation was used to extract a static archive
from MediaWiki:

```shell
#!/bin/bash

WIKI=asteroSTEP
#WIKI=corotgiantsWiki
#WIKI=hare_and_hounds1_spaceinn
#WIKI=KascWg1Wiki
#WIKI=KascWg2Wiki
#WIKI=platogiantsWiki

wget --recursive \
     --level=inf \
     --page-requisites \
     --adjust-extension \
     --convert-links \
     --no-parent \
     -R "*Special*" \
     -R "*oldid=*" \
     -R "*limit=*" \
     "http://bison.ph.bham.ac.uk/$WIKI"
```

----
