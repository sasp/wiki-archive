;----------------------------------------------------------------
;----------------------------------------------------------------
;  read_5_long_runs
;                                   by  R.A. Garcia
;----------------------------------------------------------------
;   Version:   V1.0    ;   04 June 2010
;----------------------------------------------------------------
;  Concatenate Q0-Q3 files of 5 stars
;           jpg file stored in :  "./FIGS_JPG_5long"
;           Results saved in :   "./DATA_5long"
;           The results are 2 FITS file, one containing the PSD and the other the LC
;----------------------------------------------------------------
;----------------------------------------------------------------

;----------------------------------------------------------------
;   Convert in ppm and removing 6th order poly fit
;----------------------------------------------------------------
PRO ppm,in, out
  a=poly_fit(in(0,*),in(1,*),6,fit)
  out=in
  out(1,*)=1e6*((in(1,*)/fit)-1)
END

;--------------------------------------------------------
; Save Jpeg file
;--------------------------------------------------------
PRO save_jpeg,file,help=help,quality=quality
    IF keyword_set(help) THEN BEGIN
        doc_library,'save_jpeg'
        RETURN
    ENDIF    
    qual=100
    If keyword_set(quality) THEN qual=quality
    write_jpeg,file,tvrd(true=1),true=1,quality=qual
    RETURN
END

;--------------------------------------------------------
; Convert not-a-number points to NaN
;--------------------------------------------------------
PRO BAD2NAN,data,bad_ndx=bad_ndx
    bad_ndx=WHERE(FINITE(data) ne 1)  ; Detect not-a-number values 
    IF bad_ndx(0) gt -1 THEN data(bad_ndx)= !values.f_nan   ; Change them to NaN
END

;--------------------------------------------------------
; Reading data_file in ASCII format
;--------------------------------------------------------
PRO read_kepler_ascii,name,data,stat,roll_label,no_time_cor=no_time_cor,no_nan=no_nan 
    ntc=1
    IF keyword_set(no_time_cor) THEN ntc=0
    CLOSE,2
    OPENR,2,name
    REPEAT BEGIN
        st='  '
        READF,2,st
        st1=STRMID(st,0,1)  ; Extract the first character
        IF st1 eq '#' THEN BEGIN 
            IF STRMID(st,2,7) eq 'Version' THEN BEGIN
               version=strsplit(st,':',/EXTRACT,/REGEX)
               vers=float(version(1))
            ENDIF   
            IF STRMID(st,2,6) eq 'Season' THEN BEGIN
               ROLL_st=strpos(st,'Q')   ;Position of Q
               ROLL_Label=(strmid(st,roll_st))    ; Label
            ENDIF                
        ENDIF
    ENDREP UNTIL st1 ne '#'
    CLOSE,2
    IF Vers gt 1 THEN BEGIN
       READCOL,name,v1,v2,v3,v4,v5,v6,v7,format='D,D,D,D,D,D,D',/silent, comment='#'
    ENDIF ELSE BEGIN
      ; READCOL,name,v1,v2,v3,v4,format='D,D,D,D',/silent, comment='#'   ; Version 1 only 4 columns
      ; v5=v4
      ; v5[*]=0
    ENDELSE
    time_correction=0.0d0
    Roll=LONG(STRMID(roll_label,1))
    IF  ROLL le 1 and ntc gt 0 THEN BEGIN  
        PRINT,'*********************************************************'
        PRINT,'Reading A Q',STRTRIM(Roll,1), '. Applying Time Correction'
        PRINT,'*********************************************************'
        time_correction=0.5d0 
    ENDIF    
    np=N_ELEMENTS(v1)
    data=DBLARR(5,np)
    data[0,*]=v1+time_correction
    data[1,*]=v6    ; New corrected time series
    data[2,*]=v3
    data[3,*]=v4
    data[4,*]=v5
    IF not keyword_set(no_nan) THEN BAD2NAN,data,bad_ndx=bad_ndx
    stat=N_elements(ndx)
RETURN
END


;------------------
;  MAIN
;------------------
jpg=0
PRINT,'Do you want to save a Jpg file (1=Yes)?'
READ,jpg
path=' '
PRINT,'Give me the path to the directory where all the data of the 5 stars is stored (finishing by /)'
READ,path
;path='/Users/rgarcia/DATA/KEPLER/Solar_like/RUNS/5_long_runs/'
wild='_Long'
file='kplr'
correc_stddev = 1   ; 1=Correction to have the standard dev of Q0 for the Q3 data of Scully, Cleopatra and Mulder
path_save='./DATA_5long/'
path_jpg='./FIGS_JPG_5long/'

targets=[11395018,11234888,10920273,10339342,10273246]  ; stars to be analyzed
targ_tit=['Boogie','Tigger','Scully','Cleopatra','Mulder']                    ; Cats names
ntarg=n_elements(targets)

FOR i=0l,ntarg-1 DO BEGIN
;for i=1l,1 DO BEGIN
   name_files=file_search(path,'*'+strtrim(long(targets(i)),1)+'*.dat',count=nfiles,/expand_environment)
   PRINT,name_files
   IF i eq 0 THEN sigm=dblarr(ntarg,nfiles)
   if nfiles ge 1 THEN BEGIN   
        for j=0l,nfiles-1 DO BEGIN   
         !p.multi=[0,1,2]
          name=name_files(j)
          print,'Name=',i,name
          id=strmid(file_basename(name),4,9)
          read_kepler_ascii,name,temp,stat,roll_label
          data_org=temp(0:1,*)      ;  We use only the 2 first columns
          gd=WHERE(finite(data_org(1,*)) eq 1)  ; Indexes of good points
          plot,data_org(0,*),data_org(1,*),/yn
          in=[data_org(0,gd)-data_org(0,gd(0)),data_org(1,gd)]
          ppm,in,out    ;   Convert to ppm 
          data_org(1,gd)=out(1,*)
          sigm(i,j)=stddev(data_org(1,gd),/double)  ; Compute sigma
          PRINT,j,' Std Deviation=',sigm(i,j)
          IF i ge 2 THEN BEGIN  ; Correction by the standard deviation only for Scully, Cleopatra and Mulder
             If j ge 5 THEN BEGIN   ; Correction only for Q3 data
                ref_sigma = mean(sigm(i,0:4),/double)       ; Mean of the standard deviation of Q0-Q2 
                IF correc_stddev eq 1 THEN BEGIN
                   data_org(1,*)=data_org(1,*)*ref_sigma/sigm(i,j)
                   PRINT,'Correction by Standard deviation'
                 ENDIF  
             ENDIF   
         ENDIF
         plot,data_org(0,*),data_org(1,*),/yn
         IF j eq 0 THEN BEGIN
            data=data_org
         ENDIF ELSE BEGIN
            temp=data
            np1=n_elements(temp(0,*))
            np2=n_elements(data_org(0,*))
            data=dblarr(2,np1+np2)
            data(*,0:np1-1)=temp
            data(*,np1:*)=data_org
          ENDELSE
        ENDFOR   
        !p.multi=[0,1,2]
        np=n_elements(data(0,*))
        t=reform(data(0,*)-data(0,0))*86400.   ; time in seconds with t0=0
        res=reform(data(1,*))   ;
        ndx10=lindgen(np/10.)*10
        ndx2=lindgen(np/2.)*2.
        gd=where(Finite(res) eq 1)
        !p.multi=0
        plot,t,res,/yn
        ;stop
        a=lnp_test(t(gd),res(gd),/double,wk1=freq,wk2=pot, ofac=1)
	    
    	length=max(t)/86400.
    	res_var=moment(res(gd), /double)
    	var=res_var(1)
    	pot=pot*var/total(pot, /double)/((freq(1)-freq(0))*1e6)
    	
        IF jpg eq 1 THEN BEGIN
           !p.multi=[0,1,2]
            ;plot,t,res,/yn,BACKGROUND = 255, COLOR = 0,xtitle='Time (days)',ytitle='Flux (ppm)',tit=targ_tit(i)+' '+strtrim(id,1)

            PLOT,t(ndx10)/86400.,res(ndx10),/yn,xst=1,BACKGROUND = 255, COLOR = 0,charsize=2,xtitle='Time (days)',ytitle='Flux (ppm)',tit=targ_tit(i)+' '+strtrim(id,1)
           plot_oo,freq*1e6,smooth(pot,20,/edge_truncate),xrange=[1,7e3] ,yst=1,xst=1,BACKGROUND = 255, COLOR = 0,xtitle='Frequency',ytitle='PSD (smth 20)'
           FILE_MKDIR,path_jpg
           file_jpg=path_jpg+'kplr'+id+wild+'_std.jpg'
           save_jpeg,file_jpg, quality=70
        ENDIF ELSE BEGIN 
            !p.multi=[0,1,2]
            PLOT,t(ndx10)/86400.,res(ndx10),/yn,xst=1,xtitle='Time (days)',ytitle='Flux (ppm)',tit=targ_tit(i)+' '+strtrim(id,1)
            plot_oo,freq*1e6,smooth(pot,20,/edge_truncate),xrange=[1,8e3] ,yst=1,xst=1,xtitle='Frequency',ytitle='PSD (smth 20)'
           ; If numax(0) gt -1 THEN  oplot,[numax(i),numax(i)]*1e-6,[1e-10,1e10],lin=1
        ENDELSE
        FILE_MKDIR,path_save
        PRINT,'Saving DATA'
        data_spec=dblarr(2,n_elements(freq))
        data_spec(0,*)=freq
        data_spec(1,*)=pot
        
        WRITEFITS,path_SAVE+'kplr'+id+wild+'.fits',data
        WRITEFITS,path_SAVE+'kplr'+id+wild+'_fft.fits',data_spec
     ENDIF ELSE BEGIN
        PRINT,'NOT FOUND',targets(i)
     ENDELSE
ENDFOR
   

END



