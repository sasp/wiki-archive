Function rd_iraf_spec, filename, orders, hdr

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;
 ; PURPOSE: This routine reads a wavelength calibrated spectrum in 
 ;          IRAF format. 
 ;
 ; Limitations: Very few checks on parameters is made, and this is only
 ;              valid for data rebinned onto a wavelength scale with 
 ;              constant steps.
 ; 
 ; Calling example: 
 ; 
 ;   IDL> out = rd_iraf_spec( filename [,orderinfo, hdr] ) 
 ;    Inputs: filename = name of the FIES fitsfile to view
 ;    Output: out      = structure containing lambda, flux for all orders. 
 ;                       To plot order 50 do like this:
 ;                       IDL> plot, out[50].wl, out[50].fl, xr=[5450, 5530]
 ;            orderinfo: optional, variable containing a structure with the information
 ;                       about each order, relating pixels to wavelength. 
 ;            hdr:       The actual image header
 ; 
 ; Created by: F. Grundahl, September 06 - 2006, based on help, from 
 ;            E. Stempels.
 ; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
;  im = readfits( filename, hdr, /silent ) 
  im = readfits( filename, hdr) 

 ; First we want to check if there are any empty lines in
 ; the fits header, if so, get rid of them.
   length = strlen(strcompress(hdr,/remove)) 
   zz     = where( length gt 0, complement = zz1, npt )
   if npt gt 0 then begin 
     hdr = hdr[zz]
     ;print, ' -- Removed ', fix( n_elements( zz1) ), ' lines from header'
   endif

 ; Now locate where the wavelength information begins
 ; This occurs at WAT2_...
   ss    = strmid( hdr, 0, 4 )
   index = where( ss eq 'WAT2', nwave )
   if nwave eq 0 then begin 
     print, ' -- NO WAVELENGTH INFO FOUND, Returning!! '
     stop
   endif

   ;;;;;;;;;;;;,,,

   work = hdr[ index ]  ; Extract the wavelength info to working array

   ; Get rid of the first charcters which contains the FITS keyword name 
   ; "=", "'" and some blanks.
   work = strmid( work, 11, 100 )

   ; Get rid of trailing ' from the fits header variable.
   ss   = strlen( work )
   for i=0,n_elements( work ) - 1 do begin 
    work[i] = strmid( work[i], 0, ss[i]-1)
   endfor  

   ; Make it into one long string
   text = ''
   for i=0,n_elements( work ) - 1 do begin 
    text = text + work[i]
   endfor
   ; Get rid of the first charcters before we can split based on 'spec'
   length = strlen( text )
   start  = strpos( text, 'spec1' ) 
   text   = strmid( text, start, length )

   ; Extract the orders into a vector
   orders    = strsplit( text, 'spec', /extract)

   ; Remove leading 'garbage'
   ss = strpos( orders, ' = "' )
   for i=0,n_elements( orders ) - 1 do begin 
    orders[i] =  strmid( orders[i], ss[i]+4, 100)
   endfor
   ; Remove trailing 'garbage'
   ss = strpos( orders, '"' )
   for i=0,n_elements( orders ) - 1 do begin 
    orders[i] =  strmid( orders[i], 0, ss[i] ) 
   endfor

   ; Now the variable "orders" contains the text version of the
   ; IRAF wavelength info. The next step extracts the numbers into
   ; a fltarr. 
   data = dblarr( 9, n_elements( orders ) )
    for i=0,n_elements(orders) - 1 do begin 
     dum       = strsplit( orders[i], ' ', /extract)
     data[*,i] = dum
    endfor

   ; Now put this into a nice structure.
   dummy     = { nr: 0, ordernr: 0, polynr:0, lambda0: 0.0d, dlambda: 0.0d, npix: 0.0, dum: 0, ap1: 0.0, ap2: 0.0}
   orderinfo = replicate( dummy, n_elements(orders) )

     orderinfo.nr      = fix(  reform(data[0,*]) ) ; Echelle order nr.
     orderinfo.ordernr = fix(  reform(data[1,*]) ) ; Echelle order nr. 
     orderinfo.polynr  = fix(  reform(data[2,*]) ) ; Poly degree.... 0=linear
     orderinfo.lambda0 =       reform(data[3,*])   ; wavelength of 1. pixel
     orderinfo.dlambda =       reform(data[4,*])   ; wavelength step
     orderinfo.npix    = fix(  reform(data[5,*]) ) ; Npixels in order
     orderinfo.dum     = fix(  reform(data[6,*]) ) ; I dont know what this is
     orderinfo.ap1     = float(reform(data[6,*]) ) ; 1. Aperture for background
     orderinfo.ap2     = float(reform(data[7,*]) ) ; 2. Aperture for background


     ; Last step before leaving. 
     spec   = im
     header = hdr
     orders = orderinfo

     ; Attempt to make output as a single structure with all the 
     ; data.

     ; Fill the wavelengths into an array of similar dimension as 
     ; the flux array contained in spec.

     wave  = im

     for i=0,n_elements( wave[0,*] ) - 1 do begin 
        xx        = findgen( orderinfo[i].npix )
	zpt       = orderinfo[i].lambda0
	slo       = orderinfo[i].dlambda
	wave[*,i] = zpt + xx * slo
     endfor

;    out = { wave: wave, $
;     spec: spec, $
;     orders: orderinfo }

     dum = {wl: fltarr( orderinfo[0].npix ), $
            fl: fltarr( orderinfo[0].npix )} 
     out = replicate( dum, n_elements(wave[0,*] ) )

     for i=0,n_elements( wave[0,*] ) - 1 do begin 
       out[i].wl = wave[*,i]
       out[i].fl = spec[*,i]
     endfor


     return, out 

 end
